
all: shell

shell: shell.o
	g++ -Wall -g -o shell shell.o

shell.o: shell.cpp
	g++ -c -Wall -std=c++14 -g -O0 -pedantic-errors shell.cpp

clean:
	rm -f shell
	rm -f shell.o
