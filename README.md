Project 4

Anokhi Patel
Tyiesha Holmes

## Instructions

To clean my code (i.e., remove any compiled files), type:
$ make clean

This will remove the following:
shell
shell.o

To compile my code, type:
$ make 

which will make the following:
shell

To run my code (after compiling it), type:
$ ./shell

## Notes
