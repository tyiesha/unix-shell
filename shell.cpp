/**
Authors:
Anokhi Patel
Tyiesha Holmes

Project 4: 
this project is a replica of the Unix shell
*/

#include <sys/stat.h>
#include <string>
#include <string.h>
#include <cstdlib>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/types.h>
#include <termios.h>
#include <fcntl.h>

/* A process is a single process.  */
typedef struct process{
	struct process *next;       /* next process in pipeline */
	char **argv;                /* for exec */
	pid_t pid;                  /* process ID */
	char completed;             /* true if process has completed */
	char stopped;               /* true if process has stopped */
	int status;                 /* reported status value */
} process;

/* A job is a pipeline of processes.  */
typedef struct job{
	struct job *next;           /* next active job */
	char *command;              /* command line, used for messages */
	process *first_process;     /* list of processes in this job */
	pid_t pgid;                 /* process group ID */
	char notified;              /* true if user told about stopped job */
	struct termios tmodes;      /* saved terminal modes */
	int stdin, stdout, stderr;  /* standard i/o channels */
} job;

/* The active jobs are linked into a list.  This is its head.   */
job *first_job = NULL;

std::vector<char *> cstringvec(std::vector<std::string> &stringVec);
void delcstringvec(std::vector<char *> & cstringvec);
void runVecExec(std::vector<std::string> stringargs);
void displayHelp();
void sigHandle(int signo);
void close_pipe(int pipefd[2]);

job * find_job(pid_t pgid);
void job_is_stopped (job *j);
void running(job *j);
void background(job *j, int cont);
void foreground(job *j, int cont);
int func(const char *var);

bool stopped=false;

/*
 * Program to replicate a Unix shell. Type "exit" to exit program.
 */
int main()
{
	std::vector<std::string> array;
	
	char *currentDir;
	char buff[200];
	bool run = true;
	int arrayNum = 0;
	std::string argString;
	std::string pipeStr = "|";
	int pipeLocation;
	int stdoutLocation = 0; // ">"
	int stdinLocation = 0; // "<"
	int backup, backup2, infd, outfd;
	
	std::cout.setf(std::ios::unitbuf);
	
	signal(SIGINT, sigHandle);
	signal(SIGQUIT, sigHandle);
	signal(SIGTSTP, sigHandle);
	signal(SIGTTIN, sigHandle);
	signal(SIGTTOU, sigHandle);
	signal(SIGCHLD, sigHandle);
	signal(SIGCONT, sigHandle);
	signal(SIGTERM, sigHandle);
	
	while(run == true)
	{
		int numProcess = 1;
		int numPipes = 0;
		
		do{
			std::cout << "sh:";
			
			currentDir = getcwd(buff, sizeof(buff));
			std::string shortDir(currentDir);
			std::string homePath(std::getenv("HOME"));
			std::size_t found = shortDir.find(homePath);
			shortDir.replace(found, homePath.length(), "~");
			std::cout << shortDir << "$ ";
			
			getline(std::cin, argString);
		}while(argString.empty());

		//creates job struct
		pid_t pgid=getpid();
		find_job(pgid);
		job* j= NULL;
		
		
		//the below are the actions to take if user
		//selects one of the builtin commands
		if(argString == std::string("cd")){
			chdir(std::getenv("HOME"));
			continue;
		}
		if(argString == std::string("bg")){
			if(stopped==true){
				running(j);
				background(j,1);
			}
			continue;
		}
		if(argString == std::string("exit")){
			run = false;
			exit(0);
			break;
		}
		if(argString == std::string("export")){
			const char * t=argString.c_str();
			const char * s;
			s=strdup(t);
			func(s);
			continue;
		}
		if(argString == std::string("fg")){
			if(stopped==true){
				std::cout<<"stop==true"<<std::endl;
				running(j);
				std::cout<<"out the mark job"<<std::endl;
				foreground(j,1);
				std::cout<<"out the fg"<<std::endl;
			}
			continue;
		}
		if(argString == std::string("help")){
			displayHelp();
			continue;
		}
		if(argString == std::string("jobs")){
			std::cout<<"JID STATUS\t COMMAND"<<std::endl;
		
		}
		if(argString == std::string("kill")){
			kill(SIGTERM, 0);
			break;
		}
		
		std::stringstream ss(argString);
		std::string temp;

		//parsing by space 
		while(std::getline(ss, temp, ' '))
		{
			if(temp[0] == '\"')
			{
				temp.erase(0,1);
			}
			if((temp[0] != '\\') && (temp[temp.length()-1] == '\"'))
			{
				temp.erase(temp.length()-1,1);
			}
			
			if(temp == pipeStr)
			{
				numProcess++;
				numPipes++;
				//array.push_back("@");
			}
			
			//for case liike "cat       file1 file2"
			if(temp == "\0")
			{
				temp = "*";
			}
			
			array.push_back(temp);
		}
		//to change to specific PATH for cd
		if(array.at(0) == std::string("cd"))
		{
			chdir(array.at(1).c_str());
			array.clear();
			continue;
		}
		
		//to exit with specific status number
		if(array.at(0) == std::string("exit"))
		{
			int num = atoi(array.at(1).c_str());
			exit(num);
			break;
		}
		
		//io redirection
		for(unsigned int t = 0; t < array.size(); t++){
			if(array[t] == std::string(">")){
				stdoutLocation = t;
			}
		}
		
		//pipe stuff
		if(numPipes == 0)
		{
			//execvp + fork stuff
			pid_t pid;
			if((pid = fork()) == -1)
			{
				perror("fork");
			}
			else if(pid ==0) //child process
			{
				//io redirection
				if(stdoutLocation != 0){
					umask(0);
					if((outfd = open(array.at(stdoutLocation+1).c_str(), O_WRONLY | O_CREAT, 0666)) == -1){
						perror("open");
					}
					backup = dup(STDOUT_FILENO);
					dup2(outfd, STDOUT_FILENO);
					//close(STDOUT_FILENO);
					
					runVecExec(array);
					dup2(backup, STDOUT_FILENO);
				}
				
				if(stdinLocation != 0)
				{
					if((infd = open(array.at(stdinLocation+1).c_str(), O_RDONLY)) == -1)
					{
						backup2 = dup(STDIN_FILENO);
						dup2(infd, STDIN_FILENO);
						
						runVecExec(array);
						dup2(backup2, STDIN_FILENO);
					}
				}
				
				if((stdinLocation == 0) && (stdoutLocation == 0))
				{
					runVecExec(array);
				}
				
			}
			else //parent process
			{
				waitpid(pid, nullptr, 0);
			}
			
		}
		else if(numPipes > 0){
			for(unsigned int i = 0; i< array.size(); i++){
				if(array.at(i)==pipeStr){
					
					//1 pipe
					int *pipefd = new int[numPipes];
					int pid;
					
					//find + store pipeLocation
					for(unsigned x = 0; x < array.size(); x++)
					{
						if(array[x] == pipeStr)
						{
							//arrayNum++; //skip @
							//arrayNum++; //skip |
							pipeLocation = x;
							break;
						}
						
						if(array[x] == "*")
						{
							arrayNum++;
							continue;
						}
					}
			
					//create pipe
					if(pipe(pipefd) == -1)
					{
						perror("pipe");
					}
					
					//create first child
					if((pid = fork()) == -1)
					{
						perror("fork");
					}
					else if(pid == 0)
					{
						if(dup2(pipefd[1], STDOUT_FILENO) == -1)
						{
							perror("dup2");
						}
						
						close_pipe(pipefd);
						std::vector<std::string> strargs;
						
						for(unsigned int s = 0; s < array.size(); s++)
						{
							if(array[s] == pipeStr)
							{
								break;
							}
							strargs.push_back(array[s]);
						}
						
						
						runVecExec(strargs);
						
					}
					
					// create second child
					if ((pid = fork()) == -1) 
					{
						perror("fork");
						exit(1);
					} 
					else if (pid == 0) 
					{

						if (dup2(pipefd[0], STDIN_FILENO) == -1) 
						{
							perror("dup2");
						}

						close_pipe(pipefd);

						//std::vector<std::string> strargs { "less" };
						std::vector<std::string> strargs;

						//store from array vec to strargs

						//strargs.push_back(array[3]);

						for(unsigned int s = pipeLocation+1; s < array.size(); s++)
						{
							if(array[s] == pipeStr)
							{
								break;
							}
							strargs.push_back(array[s]);
						}

						runVecExec(strargs);
					}
					  
					close_pipe(pipefd);
					// wait on last child
					waitpid(pid, nullptr, 0);
				}
			}
		}
		
		array.clear();
		
	}
	array.clear();
	return EXIT_SUCCESS;
}//main

/*
 * Function to close a pipe.
 */
void close_pipe(int pipefd [2]) {
  if (close(pipefd[0]) == -1) {
    perror("close");
    exit(EXIT_FAILURE);
  } // if
  if (close(pipefd[1]) == -1) {
    perror("close");
    exit(EXIT_FAILURE);
  } // if
} // close_pipe

/*
 * Function to convert string vector to char * vector.
 * @param &stringVec string vector
 */
std::vector<char *> cstringvec(std::vector<std::string> &stringVec)
{
	std::vector<char *> cstringvec;
	for(unsigned int i = 0; i < stringVec.size(); i++)
	{
		cstringvec.push_back(new char [stringVec.at(i).size() + 1]);
		strcpy(cstringvec.at(i), stringVec.at(i).c_str());
	}
	cstringvec.push_back(nullptr);
	return cstringvec;
}//cstringvec

/*
 * Function to delete the cstring vector when finished using.
 * @param cstringvec char * vector
 */
void delcstringvec(std::vector<char *> & cstringvec)
{
	for(unsigned int i = 0; i < cstringvec.size(); i++)
	{
		delete[] cstringvec.at(i);
	}
}//delcstringvec

/*
 * Function to run execvp with a vector.
 * @ param stringargc string vector
 */
void runVecExec(std::vector<std::string> stringargs)
{
	std::vector<char *> cstringargs = cstringvec(stringargs);
	execvp(cstringargs.at(0), &cstringargs.at(0));
	perror("execvp");
	delcstringvec(cstringargs);
	exit(EXIT_FAILURE);
}//runVecExec

/*
 * Function to display the "help" command information for builtin commands.
 */
void displayHelp()
{
	std::cout << " bg JID - resume stopped job JID." << std::endl; 
	std::cout << " cd [PATH] - Change current directory to PATH." << std::endl;
	std::cout << " exit [N]" << std::endl;
	std::cout << " export NAME[=WORD] - NAME is included in the environment of executed jobs." << std::endl;
	std::cout << " fg JID - resume job JID in the foreground." << std::endl;
	std::cout << " help - displays information about builtin commands." << std::endl;
	std::cout << " jobs - list current jobs." << std::endl;
	std::cout << " kill [-s SIGNAL] PID - kills process PID." << std::endl;
}//displayHelp

/*
 * Function to handle signals.
 * @param int signo
 */
void sigHandle(int signo){
	switch(signo)
	{
		case SIGINT:
			break;
		case SIGQUIT:
			break;
		case SIGTSTP:
			break;
		case SIGTTIN:
			break;
		case SIGTTOU:
			break;
		case SIGCHLD:
			break;
		case SIGCONT:
			break;
		case SIGTERM:
			break;
	}
}//sigHandle


/*
 * Function to find the active job with the indicated pgid.
 */
job * find_job (pid_t pgid){
	job *j;
	for (j = first_job; j; j = j->next){
		if (j->pgid == pgid)
			return j;
	}
	return NULL;
}

/*
 * Function to check if all processes in the job have stopped or completed.
 * If so, returns true.
 */
void job_is_stopped (job *j){
	process *p;
	for (p = j->first_process; p; p = p->next){
		if (!p->stopped)
			stopped=true;
	}
}

/*
 * Function to check to see if the job is running or not.
 */
void running (job *j){
	process *p;

	for (p = j->first_process; p; p = p->next)
		p->stopped = 0;
	j->notified = 0;
}

/*
 * Function to resume the job in the background.
 */
void background(job *j, int cont){
	/* Send the job a continue signal, if necessary.  */
	if (cont){
		if (kill (-j->pgid, SIGCONT) < 0)
			perror ("kill (SIGCONT)");
	}
	else
		std::cout<<"-bash: bg: current: no such job"<<std::endl;
}

/*
 * Function to resume the job in the foreground.
 */
void foreground (job *j, int cont){
	pid_t shell_pgid=getpid ();
	struct termios shell_tmodes;

	int shell_terminal = STDIN_FILENO;


	/* Put the job into the foreground.  */
	tcsetpgrp(shell_terminal, j->pgid);
	/* Send the job a continue signal, if necessary.  */
	if (cont){
		tcsetattr (shell_terminal, TCSADRAIN, &j->tmodes);
		if (kill (- j->pgid, SIGCONT) < 0)
			perror ("kill (SIGCONT)");
	}
	else
		std::cout<<"-bash: fg: current: no such job"<<std::endl;
	/* Put the shell back in the foreground.  */
	tcsetpgrp (shell_terminal, shell_pgid);

	/* Restore the shellï¿½s terminal modes.  */
	tcgetattr (shell_terminal, &j->tmodes);
	tcsetattr (shell_terminal, TCSADRAIN, &shell_tmodes);
}

/*
 * Function for exporting.
 */
int func(const char *var){
	static char *oldenv;
	const char *env_format = "TEST=%s";
	const size_t len = strlen(var) + strlen(env_format);
	char *env = (char *) malloc(len);
	if (env == NULL) {
	return -1;
	}
	int retval = snprintf(env, len, env_format, var);
	if (retval < 0 || (size_t)retval >= len) {
		std::cout<<"error"<<std::endl;
	}
	if (putenv(env) != 0) {
		free(env);
		return -1;
	}
	if (oldenv != NULL) {
		free(oldenv); /* avoid memory leak */
	}
	oldenv = env;
	return 0;
}
